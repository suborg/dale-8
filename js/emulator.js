;(function() {
  
  var config = {}, machinePaused = false
  var parts = location.search.substr(1).split('&');
  for(var i = 0; i < parts.length; i++) {
    var value = decodeURIComponent(parts[i].split('=')[1]);
    if(!isNaN(parseInt(value))) {
      value = parseInt(value);
    }
    config[parts[i].split('=')[0]] = value;
  }
  
  function bufferToBinaryString(buf) {
    let responseView = new Uint8ClampedArray(buf), l = responseView.length, s = '';
    for(let i=0;i<l;i++)
      s += String.fromCharCode(responseView[i])
    return s
  }
  
  function updateTitle(gameTitle) {
    document.getElementById('title').querySelector('h1').textContent = 'DALE-8: ' + gameTitle
  }

  function runROM(romBuffer, romName) {
    var mainCanvas = document.getElementById('mainCanvas'),
        emu = Dale(window, {
          canvas: mainCanvas,
          clockFactor: 20,
          beepShape: 'triangle',
          titleElement: document.querySelector('#title h1'),
          bgColor: '0, 24, 10',
          fgColor: '0, 255, 13',
          keyMapping: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#', 'Call', 'SoftLeft', 'SoftRight', 'Enter']
        }), baseName = romName.split('/').pop()

    document.body.classList.add('ingame')
    
    function inGameMode() {
      window.addEventListener('keydown', function(e) {
        if(e.key === 'Backspace') {
          //e.preventDefault()
          if(window.confirm('Exit DALE-8?'))
            window.close()
        }
        else if(e.key === 'ArrowUp') navigator.volumeManager.requestUp()
        else if(e.key === 'ArrowDown') navigator.volumeManager.requestDown()
      })
    }

    let quirkInfo = baseName.split('.')[1], lsq = false, stq = false
    if(quirkInfo.indexOf('l')>-1) lsq = true
    if(quirkInfo.indexOf('s')>-1) stq = true

    let progData = {
      name: baseName.split('.')[0],
      quirks: {
        lsq: lsq,
        stq: stq
      },
      data: romBuffer
    } 

    emu.run(progData)
    
    inGameMode()
  }

  var screenElement = document.getElementById('screen')
  if(config.external) {
    navigator.mozSetMessageHandler('activity', function(activityRequest) {
      let option = activityRequest.source
      if(option.name === 'open' && (option.data.type === 'application/x-bin-image' || option.data.type === 'application/x-chip8-image')) {
        var reader = new FileReader()
        reader.onload = function() {
          runROM(reader.result, option.data.filename)
        }
        reader.readAsBinaryString(option.data.blob)
      }
    })
  }
  
window.addEventListener('load', function() {
  if(config.external) {
    //nothing, all is handled by activity handler
  } else if(config.src) {
    screenElement.innerHTML = 'Loading&hellip;'
    var request = new XMLHttpRequest;
    request.onload = function() {
      runROM(bufferToBinaryString(request.response), config.src);
    };
    request.open('GET', config.src);
    request.responseType = 'arraybuffer'
    request.send();
  } else {
    var pickKeyHandler = function(e) {
      if(e.key === 'Enter' || e.key === 'Call') {
        var picker = new MozActivity({
          name: "top.luxferre.dale8.pickFile",
          data: {}
        })
        picker.onsuccess = function() {
          screenElement.innerHTML = 'Loading&hellip;'
          let reader = new FileReader()

          reader.onload = function(e) {
            window.removeEventListener('keydown', pickKeyHandler)
            runROM(reader.result, picker.result.file.name);
          }

          reader.readAsBinaryString(picker.result.file)
        }
      }
    }
    window.addEventListener('keydown', pickKeyHandler)
  }
})
})()
