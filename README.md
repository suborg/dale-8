# DALE-8: A CHIP-8 platform emulator for KaiOS

## About

This is a [CHIP-8](https://en.wikipedia.org/wiki/CHIP-8) platform emulator. It was completely created from scratch and initially was intended as a [GerdaOS](https://gerda.tech) easter egg. Now, it's released as a stand-alone application into public domain. The unpacked engine source (`dale.js`) can be found in the `src/` directory.

## Usage

The app will scan for all the files on the memory card or internal memory that have the `.ch8` extension. If the file has `.l.ch8` extension, additional `LSQ` emulation quirk will be applied (see below). If the file has `.s.ch8` extention, additional `STQ` emulation quirk will be applied (see below). Is the file has `.sl.ch8` extension, both quirks will be applied.

## Supported specification

- Screen resolution: 64x32, 8px wide sprites (no extended screen mode support)
- Color palette: monochrome (RGB value of non-black color can be pre-configured)
- Memory: 4096 bytes
- 16 operation registers: V0 to VF
- Service registers: address register I, delay timer DT and sound timer ST
- 16-digit CHR ROM (loaded at 0x80)
- 3584-byte PRG ROM (loaded at 0x200)
- Subroutine call stack with the depth of 1792 (theoretically covers all loadable memory)
- All standard 35 opcodes support (RCA-specific `0NNN` calls, except `00E0` and `00EE`, are ignored) - see list below
- Two optional CPU quirks required for some games are implemented - see below
- Monophonic sound output at configurable frequency and shape (can be changed before the emulation start, default is 440 Hz triangle)

Default keymap is as follows: 0 to 9 as is, A = `*`, B = `#`, C = `Call`, D = `SoftLeft`, `E` = `SoftRight`, `F` = `Enter` (middle).

## Supported opcode list

These are all the opcodes supported by DALE-8. The list of mnemonics is taken [from here](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM). All arithmetics is unsigned 8-bit (modulo 256). Arithmetics on the `I` register is unsigned 16-bit.

Opcode | Assembly instruction | Meaning | Notes
-------|---------------------------------|---------|------
00E0 | CLS | Clear the screen |
00EE | RET | Return from the subroutine | Does nothing if we're on the top of call stack
0nnn | SYS addr | Machine ROM call at addr | Isn't used in any modern CHIP-8 programs but used for non-standard DALE-8 specific extensions (see below)
1nnn | JP addr | Unconditional jump to addr |
2nnn | CALL addr | Call the subroutine at addr |
3xkk | SE Vx, byte | Skip next instruction if Vx == byte |
4xkk | SNE Vx, byte | Skip next instruction if Vx != byte |
5xy0 | SE Vx, Vy | Skip next instruction if Vx == Vy |
6xkk | LD Vx, byte | Set Vx = byte |
7xkk | ADD Vx, byte | Set Vx = Vx + byte |
8xy0 | LD Vx, Vy | Set Vx = Vy
8xy1 | OR Vx, Vy | Set Vx = Vx OR Vy | Bitwise OR
8xy2 | AND Vx, Vy | Set Vx = Vx AND Vy | Bitwise AND
8xy3 | XOR Vx, Vy | Set Vx = Vx XOR Vy | Bitwise XOR
8xy4 | ADD Vx, Vy | Set Vx = Vx + Vy, set VF = carry | VF is set to 1 if the result would exceed 255, set to 0 otherwise
8xy5 | SUB Vx, Vy | Set Vx = Vx - Vy, set VF = NOT borrow | VF is set to 0 if the result would be less than zero, set to 1 otherwise
8xy6 | SHR Vx {, Vy} | Set Vx = Vy >> 1, VF is set to Vy&1 before the shift | If `STQ` quirk is **on**, the instruction operates on Vx instead of Vy
8xy7 | SUBN Vx, Vy | Set Vx = Vy - Vx, set VF = NOT borrow | VF is set to 0 if the result would be less than zero, set to 1 otherwise
8xyE | SHL Vx {, Vy} | Set Vx = Vy << 1, VF is set to Vy&1 before the shift | If `STQ` quirk is **on**, the instruction operates on Vx instead of Vy
9xy0 | SNE Vx, Vy | Skip next instruction if Vx != Vy |
Annn | LD I, addr | Set I = addr |
Bnnn | JP V0, addr | Jump to location addr + V0 |
Cxkk | RND Vx, byte | Set Vx = random number AND byte | Vx = rnd(0,255) & byte 
Dxyn | DRW Vx, Vy, n | Display n-byte sprite (XOR with the video memory) starting at memory location I at (Vx, Vy), set VF = collision | VF if set to 1 if **any** existing pixel of the screen was already set to 1 and sprite overwrote it with 1, making it 0, and VF is set to 0 otherwise. If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen
Ex9E | SKP Vx | Skip next instruction if key with the value of Vx is pressed |
ExA1 | SKNP Vx | Skip next instruction if key with the value of Vx is not pressed |
Fx07 | LD Vx, DT | Set Vx to the value of delay timer register |
Fx0A | LD Vx, K | Block the execution, wait for keyboard input and store the result digit into Vx |
Fx15 | LD DT, Vx | Set delay timer register to the value of Vx | 
Fx18 | LD ST, Vx | Set sound timer register to the value of Vx |
Fx1E | ADD I, Vx | Set I = I + Vx
Fx29 | LD F, Vx | Set I = location of sprite for digit stored in Vx |
Fx33 | LD B, Vx | Store BCD representation of Vx in memory locations I, I+1, and I+2 |
Fx55 | LD [I], Vx | Store registers V0 through Vx in memory starting at location I | If `LSQ` quirk is **on**, the instruction modifies I to I + x + 1
Fx65 | LD Vx, [I] | Read registers V0 through Vx from memory starting at location I | If `LSQ` quirk is **on**, the instruction modifies I to I + x + 1

## Non-standard CHIP-8 extensions in DALE-8

All non-standard extensions in DALE-8 are implemented with `0nnn` opcode (`SYS nnnh` instruction). Hence, they will just be ignored in the implementations that don't support them.

Here are all non-standard `SYS` instructions implemented as of now:

- `SYS 666h` - set current beep frequency to the lower 6 bits of VA (represented with note from 0 to 63), and the waveform to the upper 2 bits of VA (0 - square, 1 - triangle, 2 - saw, 3 - noise). This syscall is used to extend music capabilities of CHIP-8. The note range is assuming that A4 is 440 Hz, and ranges from C2 (value 0, 65.41 Hz) to B6 (value 63, 1975.53 Hz).

## Spread the knowledge!

Both the app and the DALE-8 engine itself are public domain.
