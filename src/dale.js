function Dale(global, config = {}) {

  //poly-fcking-fills
  var freqList = new Float32Array(60), AudioContext = global.AudioContext || global.webkitAudioContext,
      RAF = global.requestAnimationFrame || global.webkitRequestAnimationFrame || global.mozRequestAnimationFrame || function xR(cb){var timeout = global.setTimeout(xR, 1000/60); cb(); return timeout},
  //Optimizer-friendly "globals":

      //config part, all config variables are optional (even canvas, if you already have one with ID "C")
      beepFrequency = parseInt(config.beepFreq || 440),           //change CHIP-8 buzzer frequency
      beepShape = config.beepShape || 'square',                 //change CHIP-8 buzzer waveform shape
      keyMapping = config.keyMapping || ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'],
      pixelColor = config.fgColor || '255, 255, 255',
      bgColor = config.bgColor || '0, 0, 0',
      titleElement = config.titleElement || null,             //place where to output the game title for extended JSON format loading
      canvas = config.canvas || global.document.getElementById('C'),
      scaleFactor = 2, //shift value, not multiplier! (2 is 4x) To speed up upscaling, it can only be done on the powers of 2
      pxlSize = 1<<scaleFactor,
      clockFactor = parseInt(config.clockFactor || 20),      //allow underclocking or overclocking by changing the amount of CPU cycles in one 60Hz loop

      //persistent part, some of which depends on config
      screenS = 2048,
      scaledW = 64<<scaleFactor,
      scaledH = 32<<scaleFactor,
      screen = new Uint8Array(screenS),
      videoCtx = canvas.getContext('2d'),
      audioCtx = new AudioContext(),
      timeStep = 1/60,
      modVxInsteadOfVyOnShift = false, //lsq quirk
      incIregOnStore = true, //stq quirk 
      stCache = 0,
      inputCache = new Uint8Array(16), //it should be zero-filled by default
      chrROM = Uint8Array.of( //sprites for storing HEX digits as per the spec in the lower protected memory area (under 0x1ff)
        0xF0, 0x90, 0x90, 0x90, 0xF0, 0x20, 0x60, 0x20, 0x20, 0x70, 0xF0, 0x10, 0xF0, 0x80, 0xF0, 0xF0, 0x10, 0xF0, 0x10, 0xF0, 0x90, 0x90, 0xF0, 0x10, 0x10, 0xF0, 0x80, 0xF0, 0x10, 0xF0, 0xF0, 0x80, 0xF0, 0x90, 0xF0, 0xF0, 0x10, 0x20, 0x40, 0x40, 0xF0, 0x90, 0xF0, 0x90, 0xF0, 0xF0, 0x90, 0xF0, 0x10, 0xF0, 0xF0, 0x90, 0xF0, 0x90, 0x90, 0xE0, 0x90, 0xE0, 0x90, 0xE0, 0xF0, 0x80, 0x80, 0x80, 0xF0, 0xE0, 0x90, 0x90, 0x90, 0xE0, 0xF0, 0x80, 0xF0, 0x80, 0xF0, 0xF0, 0x80, 0xF0, 0x80, 0x80
      ),
      chrLen = 80, chrStartAddr = 0x80, romStartAddr = 0x200,

  //For hi-res audio, the only solution here is to pre-create the oscillator and pre-mute it
  
      globalOsc = audioCtx.createOscillator(), sr = audioCtx.sampleRate, globalGain = audioCtx.createGain(),
      nGain = audioCtx.createGain(),
      noiseBuffer = audioCtx.createBuffer(1, sr*4, sr),
      nd = noiseBuffer.getChannelData(0),
      nl = noiseBuffer.length, noizer, i,
      noiseChan = false

  globalOsc.type = beepShape
  globalOsc.frequency.value = beepFrequency
  globalGain.gain.value = 0
  nGain.gain.value = 0
  globalOsc.connect(globalGain)
  globalGain.connect(audioCtx.destination)
  nGain.connect(audioCtx.destination)
  globalOsc.start()

  // fill the noise channel and note-to-frequency table
  for(i=0;i<nl;i++) {
    if(i<61) freqList[i] = 440*Math.pow(2, (i-33)/12)
    nd[i] = Math.random() * 2 - 1
  }

  ;(function noizeEnd() {
    (noizer = audioCtx.createBufferSource()).buffer = noiseBuffer
    noizer.connect(nGain)
    noizer.onended = noizeEnd
    noizer.start()
  })()

  function updateBg() {
    //Make the background color permanent for the canvas
    canvas.style.backgroundColor = 'rgb(' + bgColor + ')'
    videoCtx.fillStyle = 'rgb(' + pixelColor + ')'
  }

  //Input subsystem

  function initInput() {
    var reverseKeyMapping = {} // because indexOf is not fast enough
    for(let val=0;val<16;val++)
      reverseKeyMapping[keyMapping[val]] = val 
    global.addEventListener('keydown', function(e){
      let key = e.key
      if(key in reverseKeyMapping) {
        e.preventDefault()
        inputCache[reverseKeyMapping[key]] = 1
      }
    })
    global.addEventListener('keyup', function(e){
      let key = e.key
      if(key in reverseKeyMapping) {
        e.preventDefault()
        inputCache[reverseKeyMapping[key]] = 0
      }
    })
  }

  //Video subsystem

  function DaleRender() {
    videoCtx.clearRect(0, 0, scaledW, scaledH)
    for(let x,y,si=0;si<screenS;si++) {
      if(screen[si] === 1) {
        y = (si >> 6) << scaleFactor
        x = (si & 63) << scaleFactor
        videoCtx.fillRect(x, y, pxlSize, pxlSize)
      }
    }
  }

  //Audio subsystem

  function DaleBeep(stVal) {
    if(stVal === 0) {
      globalGain.gain.value = 0
      nGain.gain.value = 0
    } else (noiseChan ? nGain : globalGain).gain.value = 1
  }

  //System core

  function rndMask(bitmask) {
    return ((Math.random() * 256) | 0) & 255 & bitmask
  }

  function DaleCore(stdlib, foreign, heap) {

    //Memory model

    var ram = new stdlib.Uint8Array(4096), //0x000..0xFFF
        stack = new stdlib.Uint16Array(1792), //subroutine call stack (of addresses, so it's 16-bit), 1792 = (4096 - 0x200) / 2
        sp = 0,                           //subroutine call stack pointer
        V = new stdlib.Uint8Array(16),  //registers: V0..VF
        iReg = 0,                   //I: 0x0000..0xFFFF
        dtReg = 0,                 //delay timer register, 0..255, decreases every 1/60 sec
        stReg = 0,                 //sound timer register, 0..255, decreases every 1/60 sec
        pc = romStartAddr,         //program counter
        renderScheduled = 0,       //whether we have scheduled the rendering for next animation cycle
        paused = 0,                //whether we are paused or not
        skip = 0,                  //whether we are skipping the next instruction or not
        b1 = 0, b2 = 0, d1 = 0, d2 = 0, d3 = 0, d4 = 0, nnn = 0   //different opcode parts

    function clearScreen() {
      screen = new Uint8Array(screenS)
      renderScheduled = 1
    }

    function timerLoops() {
      if(dtReg > 0) dtReg--;
      if(stReg > 0) {
        stReg--;
        DaleBeep(stReg);
      }
    }

    function drawSprite(x, y, bLen) {
      x &= 63
      y &= 31
      let i=0, startPoint = (y<<6) + x, setVf = 0;
      let spriteLen = bLen << 3, spriteArr = new Uint8Array(spriteLen)
      for(let i=0;i<bLen;i++) {
        let realbyte = ram[iReg + i], ind = i << 3
        spriteArr[ind] = realbyte >>> 7
        spriteArr[ind + 1] = (realbyte >>> 6) & 1
        spriteArr[ind + 2] = (realbyte >>> 5) & 1
        spriteArr[ind + 3] = (realbyte >>> 4) & 1
        spriteArr[ind + 4] = (realbyte >>> 3) & 1
        spriteArr[ind + 5] = (realbyte >>> 2) & 1
        spriteArr[ind + 6] = (realbyte >>> 1) & 1
        spriteArr[ind + 7] = realbyte & 1
      }
      for(let i=0;i<spriteLen;i++) {
        let newPixel = spriteArr[i],
            baseIndex = (i>>3)<<6,
            nextLineIndex = baseIndex + 64
            index = startPoint + baseIndex + (i&7)
        if(index >= nextLineIndex) //horizontal wrap
          index -= 64
        if(index >= screenS) //vertical wrap
          index -= screenS
        let oldPixel = screen[index]
        if(newPixel === 1 && oldPixel === 1) setVf = 1
        screen[index] ^= newPixel
      }
      V[0xf] = setVf
      renderScheduled = 1
    }

    function waitForInput(cb) {
      var waiter = function() {
        for(let i=0;i<16;i++) {
          if(inputCache[i] === 1) {
            cb(i); return
          }
        }
        RAF(waiter)
      }
      waiter()
    }

    function cpuLoop() {
      if(skip) { //skip once if marked so
        pc += 2
        skip = 0
      }
      b1 = ram[pc++]    //read the first byte and advance the counter
      b2 = ram[pc++]    //read the second byte and advance the counter
      d1 = b1 >> 4     //extract the first instruction digit
      d2 = b1 & 15     //extract the second instruction digit
      d3 = b2 >> 4     //extract the third instruction digit
      d4 = b2 & 15     //extract the fourth instruction digit
      nnn = (d2 << 8) | b2 //extract the address for NNN style instructions
      // Main challenge begins in 3... 2... 1...
      switch(d1) {
        case 0: //omit everything except 00E0 and 00EE!
          if(d2 === 0 && d3 === 0xE) {
            if(d4 === 0) clearScreen() //pretty obvious, isn't it?
            else if(d4 === 0xE && sp > 0) {  //return from the subroutine
              pc = stack[--sp]
            }
          }
          else if(d2 === 6 && d3 === 6 && d4 === 6) { //non-standard SYS 666h: set current beep frequency to the lower 6 bits of VA (represented with note from 0 to 63),
                                                      //and the waveform to the upper 2 bits of VA (0 - square, 1 - triangle, 2 - saw, 3 - sine)
            globalOsc.frequency.value = freqList[V[0xa] & 63]
            nnn = V[0xa]>>6
            if(nnn === 3) //GIMME_NOIZE
              noiseChan = 1
            else {
              noiseChan = 0
              globalOsc.type = ['square', 'triangle', 'sawtooth'][V[0xa]>>6]
            }
          }
          break;
        case 1: //unconditional jumpstyle
          pc = nnn
        break;
        case 2: //subroutine call
          stack[sp++] = pc
          pc = nnn
        break; 
        case 3: //Skip the following instruction if the value of register V{d2} equals {b2}
          if(V[d2] === b2)
            skip = 1
        break;
        case 4: //Skip the following instruction if the value of register V{d2} is not equal to {b2}
          if(V[d2] !== b2)
            skip = 1
        break;
        case 5:
          if(d4 === 0 && V[d2] === V[d3]) //Skip the following instruction if the value of register V{d2} equals V{d3}
            skip = 1
        break;
        case 6: //Store number {b2} in register V{d2}
          V[d2] = b2
        break;
        case 7: //Add the value {b2} to register V{d2}
          V[d2] += b2
        break;
        case 8: //Monster #1
          switch(d4) { //for all instructions in this section, d4 is the selector and d2 and d3 are the X and Y parameters respectively
            case 0: //Store the value of register VY in register VX
              V[d2] = V[d3]
            break;
            case 1: //Set VX to VX OR VY
              V[d2] |= V[d3]
            break;
            case 2: //Set VX to VX AND VY
              V[d2] &= V[d3]
            break;
            case 3: //Set VX to VX XOR VY
              V[d2] ^= V[d3]
            break;
            case 4: //Add the value of register VY to register VX with overflow recorded in VF
              nnn = V[d2] + V[d3]
              V[0xf] = (nnn > 255) ? 1 : 0
              V[d2] = nnn & 255
            break;
            case 5: //Set VX = VX - VY with underflow recorded in VF
              nnn = V[d2] - V[d3]
              V[0xf] = (nnn < 0) ? 0 : 1
              V[d2] = (nnn + 256) & 255
            break;
            case 6: //Store the value of register VY shifted right one bit in register VX, set register VF to the least significant bit prior to the shift
              if(modVxInsteadOfVyOnShift) {
                V[0xf] = V[d2]&1
                V[d2] = V[d2]>>>1
              }
              else {
                V[0xf] = V[d3]&1
                V[d2] = V[d3]>>>1
              }
            break;
            case 7: //Set VX = VY - VX with underflow recorded in VF
              nnn = V[d3] - V[d2]
              V[0xf] = (nnn < 0) ? 0 : 1
              V[d2] = (nnn + 256) & 255
            break;
            case 0xe: //Store the value of register VY shifted left one bit in register VX, set register VF to the most significant bit prior to the shift
              if(modVxInsteadOfVyOnShift) {
                V[0xf] = V[d2]>>>7
                V[d2] = V[d2]<<1
              }
              else {
                V[0xf] = V[d3]>>>7
                V[d2] = V[d3]<<1
              }
            break;
          }
        break;
        case 9:
          if(d4 === 0 && V[d2] !== V[d3]) //Skip the following instruction if the value of register V{d2} is not equal to the value of register V{d3}
            skip = 1
        break;
        case 0xa: //Store memory address NNN in register I
          iReg = nnn
        break;
        case 0xb: //Jump to address NNN + V0
          pc = nnn + V[0]
        break;
        case 0xc: //Set V{d2} to a random number with a mask of {b2}
          V[d2] = rndMask(b2)
        break;
        case 0xd:
          /*
            Draw a sprite at position V{d2}, V{d3} with {d4} bytes of sprite data starting at the address stored in I
            Set VF to 01 if any set pixels are changed to unset, and 00 otherwise
          */
          drawSprite(V[d2], V[d3], d4)
        break;
        case 0xe:
          if(b2 === 0x9e) { //Skip the following instruction if the key corresponding to the hex value currently stored in register V{d2} is pressed
            if(inputCache[V[d2]] === 1)
              skip = 1
          }
          else if(b2 === 0xa1) { //Skip the following instruction if the key corresponding to the hex value currently stored in register V{d2} is not pressed
            if(inputCache[V[d2]] === 0)
              skip = 1
          }
        break;
        case 0xf: //Monster #2
          switch(b2) { //d2 is the parameter X for all these instructions, b2 is the selector
            case 0x07: //Store the current value of the delay timer in register VX
              V[d2] = dtReg
            break;
            case 0x0a: //Wait for a keypress and store the result in register VX
              paused = 1
              waitForInput(function(c) {
                V[d2] = c
                paused = 0
              }) 
            break;
            case 0x15: //Set the delay timer to the value of register VX
              dtReg = V[d2]
            break;
            case 0x18: //Set the sound timer to the value of register VX
              stReg = V[d2]
            break;
            case 0x1e: //Add the value stored in register VX to register I
              iReg += V[d2]
            break;
            case 0x29: //Set I to the memory address of the sprite data corresponding to the hexadecimal digit stored in register VX
              nnn = V[d2]&15
              iReg = chrStartAddr + (nnn<<2) + nnn
            break;
            case 0x33: //Store the binary-coded decimal equivalent of the value stored in register VX at addresses I, I+1, and I+2
              nnn = V[d2]
              ram[iReg] = (nnn / 100) | 0
              ram[iReg + 1] = ((nnn % 100) / 10) | 0
              ram[iReg + 2] = nnn % 10
            break;
            case 0x55:
              /*
                Store the values of registers V0 to VX inclusive in memory starting at address I
                I is set to I + X + 1 after operation
              */
              for(nnn=0;nnn<=d2;nnn++)
                ram[iReg+nnn] = V[nnn]
              if(incIregOnStore) iReg += nnn
            break;
            case 0x65:
              /*
                Fill registers V0 to VX inclusive with the values stored in memory starting at address I
                I is set to I + X + 1 after operation
              */
              for(nnn=0;nnn<=d2;nnn++)
                V[nnn] = ram[iReg+nnn]
              if(incIregOnStore) iReg += nnn
            break;
          }
        break;
      }
    }

    function reset() {
      paused = 1 //pre-pause to stop the timers
      //zero-out RAM to avoid memory attacks
      ram.fill(0)
      //fill the character ROM part
      for(let i=0; i<chrLen; i++)
        ram[i+chrStartAddr] = chrROM[i]
      pc = romStartAddr
      V.fill(0)
      stack.fill(0)
      iReg = 0
      sp = 0
      skip = 0
      stReg = 0
      dtReg = 0
      renderScheduled = 0
    }

    function start() {
      paused = 0
      RAF(function runner() {
        if(!paused) {
          for(var i=0; i<clockFactor; i++) {
            cpuLoop()
          }
          if(renderScheduled) {
            DaleRender()
            renderScheduled = 0
          }
          timerLoops()
        } 
        RAF(runner)
      })
    }

    return {
      run: function(bin, l, lsq, stq) {
        reset()
        //fill the program ROM part
        for(let i=0; i<l; i++) {
          ram[romStartAddr + i] = bin[i]
        }
        //fill the quirks
        incIregOnStore = lsq ? true : false
        modVxInsteadOfVyOnShift = stq ? true : false
        //start the VM
        start()
      },
      pause: function(a) {
        paused = a ? a&1 : 1
        if(!paused) timerLoops()
      },
      reset: reset
    }

  }

  function decodeROM(rom) { //dec0de ROM binary string (or base64) into a byte array
    let bin = null
    try {
      bin = atob(rom)
    }
    catch(e) {
      bin = rom
    }
    return new Uint8Array(bin.split('').map(x=>x.charCodeAt(0)))
  }

  function decodeJSON(romJson) { //parse and fill the configuration from ROM json
    let obj = null
    try {
      obj = JSON.parse(romJson)
    }
    catch(e) { //ROM is not in JSON format
      if(romJson instanceof Object)
      obj = romJson
      else return null
    }
    if(obj) {
      if(!obj.data) return null
      let lsq = false, stq = false
      if(obj.name && titleElement)
        titleElement.textContent = obj.name
      if(obj.palette) {
        if(obj.palette.bg)
          bgColor = obj.palette.bg
        if(obj.palette.fg) 
          pixelColor = obj.palette.fg 
      }
      if(obj.quirks) {
        if(obj.quirks.lsq) lsq = true
        if(obj.quirks.stq) stq = true
      }
      return [obj.data, lsq, stq]
    }
  }
  
  function run(rom, lsq=false, stq=false) {
    let dec0ded = decodeROM(rom), l = dec0ded.length
    if(dec0ded && l < 0x1fff - romStartAddr) {
      initInput()
      let daleCoreInst = DaleCore(global)
      daleCoreInst.run(dec0ded, l, lsq, stq)
      return daleCoreInst
    }
    else { //ROM too large or of invalid format or whatever
      return null;
    }
  }


  return {
    run: function(rom, lsq=false, stq=false) {
      let romObj = decodeJSON(rom)
      updateBg()
      if(romObj)
        return run(romObj[0], romObj[1], romObj[2])
      else {
        return run(rom, lsq, stq)
      }
    }
  }

}
